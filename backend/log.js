

function info(string) {
  console.log(`${new Date().toISOString()} - ${string}`);
}


function error(string, exception) {
  console.error(`${new Date().toISOString()} - ${string}`, exception);
}

exports.info = info;
exports.error = error;