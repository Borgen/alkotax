const { MongoClient } = require("mongodb");
const log = require('./log');

const mongoUser = process.env.ALKOTAX_MONGOUSER;
const mongoPassword = process.env.ALKOTAX_MONGOPASS;
const mongoHost = process.env.ALKOTAX_MONGOHOST;

log.info(`Connecting to MongoDB host '${mongoHost}'`);
const uri = `mongodb://${mongoUser}:${mongoPassword}@${mongoHost}:27017`;
const client = new MongoClient(uri, { useUnifiedTopology: true });

async function getCollection(collection) {
  if (!client.isConnected())
      await client.connect();

  const database = client.db('alkotax');
  return database.collection(collection);
} 

async function loadQueryResult(queryString) {
  const collection = await getCollection('queries');
  const storedResult = await collection.findOne({ _id: queryString });
  return storedResult;
}

async function incrementCount(mongoModel) {
  const collection = await getCollection('queries');
  await collection.updateOne({ _id: mongoModel._id }, { $inc: { count: 1 } });
}

async function saveQueryResult(queryResult) {
  try {
    const collection = await getCollection('queries');
    const mongoModel = { ...queryResult, _id: queryResult.queryString, lastSaved: new Date() };
    await collection.updateOne({ _id: mongoModel._id }, { $set: mongoModel }, { upsert: true });

    log.info(`Saved query result '${mongoModel._id}'`);
  } catch (e) {
    log.error("COULD NOT SAVE", e);
  }
}

async function saveProducts(products) {
  try {
    const collection = await getCollection('products');

    for (let i = 0; i < products.length; i++) {
      const productModel = { ...products[i] };
      productModel._id = productModel.productId; // This is the public facing article number. Renat 70cl = "1".
      await collection.updateOne({ _id: productModel._id }, { $set: productModel }, { upsert: true });
    }

    log.info(`Saved ${products.length} products`);
  } catch (e) {
    log.error("COULD NOT SAVE", e);
  }
}

process.on('exit', async function() {
  console.log("Closing mongodb connection");
  await client.close();
});

exports.loadQueryResult = loadQueryResult;
exports.incrementCount = incrementCount;
exports.saveQueryResult = saveQueryResult;
exports.saveProducts = saveProducts;