const systemet = require('./systembolagetClient');
const mongo = require('./mongodb');
const log = require('./log');
const differenceInHours = require('date-fns/differenceInHours');

async function fetchAndSaveQuery(queryString, storedResult) {
  log.info('Getting data from systembolaget', queryString);
  const { products, hasMore } = await systemet.fetchProducts(queryString);
  log.info(`Found ${products.length} products`, queryString);
  const result = { queryString: queryString, products: products, hasMore: hasMore };

  if (result.products && result.products.length > 0) {
    // This can happen async
    const oldCount = storedResult && Number.isInteger(storedResult.count) ? storedResult.count : 0; 
    mongo.saveQueryResult({ ...result, count: oldCount + 1 });
    mongo.saveProducts(result.products);
  }

  return result;
} 

function createResponseModel(storedResult) {
  return { queryString: storedResult.queryString, products: storedResult.products, hasMore: !!storedResult.hasMore };
}

async function getProductsWithQuery(queryString) {
  queryString = queryString.toLowerCase();
  log.info(`Searching for '${queryString}'`);
  const storedResult = await mongo.loadQueryResult(queryString);

  if (storedResult) {
    if (differenceInHours(new Date(), storedResult.lastSaved) < 24) {
      log.info('Found stale data in db', queryString);
      fetchAndSaveQuery(queryString, storedResult);
    } else {
      log.info('Found fresh data in db', queryString);
      mongo.incrementCount(storedResult);
    }

    return createResponseModel(storedResult);
  }

  // New query. Get it and return it.
  return createResponseModel(await fetchAndSaveQuery(queryString, null));
}

exports.getProductsWithQuery = getProductsWithQuery;