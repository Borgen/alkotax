const fetch = require('node-fetch');

async function fetchProducts (queryString) {
  let pageNumber = 1;
  let hasMoreProducts = true;
  let productsArray = [];

  const firstPageData = await fetchProductsOnPage(queryString, pageNumber++);
  const totalProductCount = firstPageData.metadata.docCount;
  productsArray = firstPageData.products;

  while (hasMoreProducts && productsArray.length < totalProductCount && pageNumber <= 10) {
    const pageData = await fetchProductsOnPage(queryString, pageNumber++);
    if (pageData.products && pageData.products.length > 0) {
      hasMoreProducts = true;
      productsArray = productsArray.concat(pageData.products);
    } else {
      hasMoreProducts = false;
    }
  }

  return { products: productsArray, hasMore: productsArray.length < totalProductCount };
}

async function fetchProductsOnPage(queryString, pageNumber) {
  const queryUrl = "https://api-extern.systembolaget.se/sb-api-ecommerce/v1/productsearch/search?size=15&page=" + pageNumber + "&textQuery=" + queryString;
  const response = await fetch(queryUrl, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'Ocp-Apim-Subscription-Key': '874f1ddde97d43f79d8a1b161a77ad31',
    },
  });

  return await response.json();
}

exports.fetchProducts = fetchProducts;