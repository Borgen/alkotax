exports.variables = {
  systembolaget: {
    categories: {
      spirit: "Sprit",
      beer: "Öl",
      wine: "Vin",
      cider: "Cider & blanddrycker",
      nonAlcoholic: "Alkoholfritt"
    },
    mellanklassCategories: [
      "Sherry & Montilla",
      "Portvin",
      "Övrigt starkvin"
    ],
    percentageCut: .17,
    unitCuts: {
      wine: 4.92,
      wineSmall: 2.46,
      wineNonAlcoholic: 5.21,
      spirit: 5.52,
      cider: 1.44,
      beer: 0.84,
      beerAndCiderNonAlcoholic: 2.17
    }
  },
  skatteverket: {
    beer: [
      {
        alcFrom: 0,
        alcTo: 2.8,
        taxPerPercentageAndLiter: 0
      },
      {
        alcFrom: 2.8,
        alcTo: 100,
        taxPerPercentageAndLiter: 2.02
      }
    ],
    wine: [
      {
        alcFrom: 0,
        alcTo: 2.25,
        taxPerLiter: 0
      },
      {
        alcFrom: 2.25,
        alcTo: 4.5,
        taxPerLiter: 9.19
      },
      {
        alcFrom: 4.5,
        alcTo: 7,
        taxPerLiter: 13.58
      },
      {
        alcFrom: 7,
        alcTo: 8.5,
        taxPerLiter: 18.69
      },
      {
        alcFrom: 8.5,
        alcTo: 15,
        taxPerLiter: 26.18
      },
      {
        alcFrom: 15,
        alcTo: 18,
        taxPerLiter: 54.79
      }
    ],
    mellanklass: [
      {
        alcFrom: 1.2,
        alcTo: 15,
        taxPerLiter: 32.99
      },
      {
        alcFrom: 18,
        alcTo: 22,
        taxPerLiter: 54.79
      }
    ],
    spirit: [
      {
        alcFrom: 0,
        alcTo: 1.2,
        taxPerLiter: 0
      },
      {
        alcFrom: 1.2,
        alcTo: 100,
        taxPerLiter: 516.59
      }
    ]
  }
};