const express = require('express');
const cors = require('cors');
const productsApi = require('./backend/productsApi');
const variables = require('./backend/variables');
const app = express();
const port = 3001;

app.use(cors());
app.use('/dist', express.static('dist'));

function serveIndex(_, res) {
  res.sendFile('./dist/index.html', { root: __dirname });
}

async function serveSystembolaget(req, res) {
  const queryString = req.params.searchQuery;
  const result = await productsApi.getProductsWithQuery(queryString);
  res.setHeader('Content-Type', 'application/json');
  res.end(JSON.stringify(result));
}

async function serveVariables(_, res) {
  res.setHeader('Content-Type', 'application/json');
  res.end(JSON.stringify(variables.variables));
}

app.get('/products/:searchQuery', serveSystembolaget);
app.get('/variables', serveVariables);
app.get('/', serveIndex);
app.get('/*', serveIndex);
app.listen(port, () => console.log(`App listening on port ${port}!`));