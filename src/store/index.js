import Vue from 'vue'
import Vuex from 'vuex'
import productMapper from './productMapper';

Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
    latestQuery: "",
    currentSearchResult: [],
    sortFunc: (p1, p2) => { return p1.relevance - p2.relevance; },
    isLoadingProducts: false, 
    hasMore: false,

    variables: {},
  },
  mutations: {
    setSortFunc (state, sortFunc) {
      state.sortFunc = sortFunc;
    },
    setResultSet (state, result) {
      state.currentSearchResult = result.products.map((product, index) => productMapper(product, index, state.variables));
      state.hasMore = result.hasMore;
      state.isLoadingProducts = false;
    },
    startLoading (state, query) {
      state.latestQuery = query;
      state.isLoadingProducts = true;
      state.hasMore = false;
    },
    setVariables (state, variables) {
      state.variables = variables;
    }
  },
  getters: {
    products: state => {
      return state.currentSearchResult.sort(state.sortFunc);
    },
    hasMoreResults: state => state.hasMore,
    isLoading: state => state.isLoadingProducts,
    latestQuery: state => state.latestQuery,
  },
  actions: {
    async loadVariables (context) {
      const queryUrl = "https://skatt.herrborgstrom.se/variables";
      const response = await fetch(queryUrl, { method: 'GET' });
      const variablesResponse = await response.json();
      context.commit('setVariables', variablesResponse);
    },
    async search (context, query) {
      context.commit('startLoading', query);
      const queryUrl = "https://skatt.herrborgstrom.se/products/" + query;
      const response = await fetch(queryUrl, { method: 'GET' });
      const systembolagetResponse = await response.json();
      context.commit('setResultSet', systembolagetResponse);
    },
    setSortFunction (context, sortFunc) {
      context.commit('setSortFunc', sortFunc);
    }
  }
});

export default store;