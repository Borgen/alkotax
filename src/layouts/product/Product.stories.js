import AlkoTaxProduct from './Product';

// More on default export: https://storybook.js.org/docs/vue/writing-stories/introduction#default-export
export default {
  title: 'Layout/Product',
  component: AlkoTaxProduct,
  // More on argTypes: https://storybook.js.org/docs/vue/api/argtypes
  argTypes: {
  },
};

// More on component templates: https://storybook.js.org/docs/vue/writing-stories/introduction#using-args
const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { AlkoTaxProduct },
  template: '<AlkoTaxProduct v-bind="$props" />',
});

const absolutVodka = {"relevance":0,"id":"1009731","name":"Explorer Vodka","subName":"Jordgubb Lime","brand":"Altia","totalPrice":219,"volumeMl":700,"alcoholPercentage":37.5,"category":"Sprit","category2":"Smaksatt sprit","category3":"Smaksatt vodka","category4":null,"categories":["Sprit","Smaksatt sprit","Smaksatt vodka",null],"imgSrc":"https://product-cdn.systembolaget.se/productimages/1009731/1009731_200.png","meta":{"isNonAlcoholic":false,"isSmallPackage":false,"systembolagetType":"Sprit","skatteverketType":"Sprit"},"vat":43.8,"alcoholTax":135.6,"systembolagetUnitCut":5.52,"systembolagetPercentageCut":4.95,"originalPrice":29.129999999999995}

export const Primary = Template.bind({});
// More on args: https://storybook.js.org/docs/vue/writing-stories/args
Primary.args = {
  product: absolutVodka
};