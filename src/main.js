import Vue from 'vue'
import VueRouter from 'vue-router';
import Store from './store';

Vue.config.productionTip = false
Vue.use(VueRouter);

import App from './App.vue';
import SearchResult from './pages/SearchResult.vue';
import About from './pages/About.vue';
import Home from './pages/Home.vue';

const router = new VueRouter({
  mode: 'history',
  routes: [
    {
      path: '/search/:searchQuery',
      name: 'search',
      component: SearchResult,
    },
    {
      path: '/about',
      name: 'about',
      component: About,
    },
    {
      path: '/',
      name: 'home',
      component: Home,
    }
  ]
});

new Vue({
  router,
  render: h => h(App),
  store: Store,
}).$mount('#app')
