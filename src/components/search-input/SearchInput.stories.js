import AlkoTaxSearchInput from './SearchInput';

export default {
  title: 'Components/SearchInput',
  component: AlkoTaxSearchInput,
  argTypes: {
    big: { control: 'boolean' },
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { AlkoTaxSearchInput },
  template: '<AlkoTaxSearchInput @onSearch="onSearch" v-bind="$props" />',
});

export const Primary = Template.bind({});
Primary.args = {
  big: true
};