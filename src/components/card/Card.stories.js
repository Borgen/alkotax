import AlkoTaxCard from './Card';

export default {
  title: 'Components/Card',
  component: AlkoTaxCard,
  argTypes: {
    clickable: { control: 'boolean' },
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { AlkoTaxCard },
  template: '<AlkoTaxCard v-bind="$props">Detta är ett kord</AlkoTaxCard>',
});

export const Primary = Template.bind({});
Primary.args = {
  clickable: false
};