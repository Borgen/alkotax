import AlkoTaxProductCard from './ProductCard';

export default {
  title: 'Components/ProductCard',
  component: AlkoTaxProductCard,
  argTypes: {
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { AlkoTaxProductCard },
  template: '<AlkoTaxProductCard v-bind="$props" />',
});

export const Primary = Template.bind({});
Primary.args = {
};