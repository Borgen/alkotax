import AlkoTaxFlaskIcon from './FlaskIcon';

export default {
  title: 'Components/Icons/FlaskIcon',
  component: AlkoTaxFlaskIcon,
  argTypes: {
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { AlkoTaxFlaskIcon },
  template: '<AlkoTaxFlaskIcon />',
});

export const Primary = Template.bind({});
Primary.args = {
};