import AlkoTaxCookieIcon from './CookieIcon';

export default {
  title: 'Components/Icons/CookieIcon',
  component: AlkoTaxCookieIcon,
  argTypes: {
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { AlkoTaxCookieIcon },
  template: '<AlkoTaxCookieIcon />',
});

export const Primary = Template.bind({});
Primary.args = {
};