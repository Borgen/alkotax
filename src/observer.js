


function getCallbackForTarget(target) {
  return callbacks.find((c) => c.element === target).callback;
}

function callback(entries) {
  entries.forEach((entry) => {
    const realCallback = getCallbackForTarget(entry.target);
    realCallback(entry);
  });
}

const observer = new IntersectionObserver(callback, {
  threshold: [.2]
});

const callbacks = [];

export function observe(element, callback) {
  callbacks.push({ element, callback });
  observer.observe(element);
}

export function unobserve(element) {
  observer.unobserve(element);
}