const path = require("path");
const WebpackAssetsManifest = require('webpack-assets-manifest')


module.exports = {
  outputDir: path.resolve(__dirname, "./dist"),
  publicPath: process.env.NODE_ENV === 'production' ? "/dist/" : undefined,
  configureWebpack: config => {
    config.plugins = config.plugins.concat(
      new WebpackAssetsManifest({
        output: 'assets.json'
      })
    );
  },
  chainWebpack: config => {
    config.module
      .rule('markdown')
      .test(/\.md$/)
      .use('file-loader')
        .loader('file-loader')
        .end();
  },
  pages: {
    index: {
      entry: 'src/main.js',
      template: 'public/index.html',
      filename: 'index.html',
      title: 'Alkoholskatt',
    },
  }
}